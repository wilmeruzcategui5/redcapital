<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompraDetalle extends Model
{
    protected $table ="detalle_compra";

    public function categoria()
    {
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }
}
