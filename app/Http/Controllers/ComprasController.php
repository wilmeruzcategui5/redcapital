<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Compra;
use App\CompraDetalle;

class ComprasController extends Controller
{
    function index(Request $req){
        $compras = Compra::whereHas('detalle', function($query) use ($req){
                $query->wherehas('categoria', function($q) use ($req){
                    // Si envío la categoria que filtre
                    if($req->categoria_id)
                        $q->where('id', $req->categoria_id);

                })->with('categoria');
            })->with('detalle')->get();

        if($req->ajax())
            return view('compras', compact('compras'));
        else{
            $categorias = Categoria::all();
            return view('index', compact('compras', 'categorias'));
        }
    }
}
