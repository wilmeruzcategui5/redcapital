<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Redcapital</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container text-center p-5">
        <h2> Test de Red Capital</h2>    
        <span style="font-size: 11px; color: gray"> Programador: Wilmer Uzcátegui / wilmeruzcategui5@hotmail.com / +56-988405863</span>    
        <div class="row">
            <div class="col-md-5 text-left">
                <form action="">
                <label for="categoria_id">Categoria (Request Normal)</label>
                <div class="input-group">
                    <select class="custom-select" id="categoria_id" name="categoria_id"> 
                        <option value="">Selecione...</option>
                        @foreach ($categorias as $categoria)
                            <option value="{{ $categoria->id }}" @if(\Request::input('categoria_id') == $categoria->id) selected @endif> {{ $categoria->nombre }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">Filtrar</button>
                    </div>
                </div>
                </form>
            </div>
            <div class="offset-2 col-md-5 text-right">
                <label for="categoria_id">Categoria (Request Ajax)</label>
                <div class="input-group">
                    <select class="custom-select" id="categoria"> 
                        <option value="">Selecione...</option>
                        @foreach ($categorias as $categoria)
                            <option value="{{ $categoria->id }}"> {{ $categoria->nombre }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" onclick="filtrar()">Filtrar</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="compras">
            @include('compras')
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        function filtrar(){
            $.ajax({
                type: "post",
                url: "{{ route('ajaxFiltro') }}",
                data: {
                    'categoria_id': $('#categoria').val(),
                    '_token': '{{ csrf_token() }}'
                },
                success: function (tabla) {
                    $('#compras').html(tabla);
                }
            });
        }
    </script>
</body>
</html>