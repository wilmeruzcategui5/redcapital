<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Producto</th>
            <th>Nombre</th>
            <th>Categoria</th>
            <th>Cantidad</th>
            <th>Precio</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody id="cuerpo">
        @php($total = 0)
        @foreach ($compras as $compra)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $compra->producto }}</td>
            <td>{{ $compra->detalle->nombre }}</td>
            <td>{{ $compra->detalle->categoria->nombre }}</td>
            <td>{{ $compra->cantidad }}</td>
            <td>{{ $compra->detalle->precio }}</td>
            <td>{{ $compra->detalle->precio * $compra->cantidad }}</td>
            @php($total += $compra->detalle->precio * $compra->cantidad)
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="6" class="text-right"> Total General </th>
            <th> {{ $total }} </th>
        </tr>
    </tfoot>
</table>