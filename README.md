## Test para RedCapital realizado por Wilmer Uzcátegui

Pasos para la instalación.

1. Crear una base de datos MYSQL
2. Clonar el repositorio y ejecutar el comando composer install
3. Configurar el archivo .env con las credenciales de la base de datos creada
4. Ejecutar el comando: php artisan migrate:refresh --seed
5. Levantar el servidor virual con php artisan serve.

Saludos.