<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesParaPruebaRedcapital extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nombre');
            $table->timestamps();
        });

        Schema::create('compras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad')->unsigned();
            $table->text('producto');
            $table->timestamps();
        });

        Schema::create('detalle_compra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('compra_id')->unsigned();
            $table->integer('categoria_id')->unsigned();
            $table->text('nombre');
            $table->double('precio', 15, 2);
            $table->timestamps();
            $table->foreign('compra_id')->references('id')->on('compras')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('detalle_compra');
        Schema::dropIfExists('compras');
        Schema::dropIfExists('categorias');
    }
}
