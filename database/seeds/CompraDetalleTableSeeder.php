<?php

use Illuminate\Database\Seeder;

class CompraDetalleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CompraDetalle::class, 50)->create();
    }
}
