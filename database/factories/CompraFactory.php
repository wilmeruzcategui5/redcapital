<?php

use Faker\Generator as Faker;

$factory->define(App\Compra::class, function (Faker $faker) {
    return [
        'cantidad' => rand(1,10),
        'producto' => $faker->text(15)
    ];
});
