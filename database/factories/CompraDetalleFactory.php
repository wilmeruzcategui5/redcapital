<?php

use Faker\Generator as Faker;

$factory->define(App\CompraDetalle::class, function (Faker $faker) {
    return [
        'compra_id' => factory(App\Compra::class)->create()->id,
        'categoria_id' => rand(1,10),
        'nombre' => $faker->text(10),
        'precio' => $faker->randomFloat(2, 0, 1000)
    ];
});
